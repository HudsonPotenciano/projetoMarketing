| ![alt text](https://docs.google.com/uc?id=1a79otEH-WjaLJr0y6i4ZvKcmn3hjVcFn) |
| ------ |
|<h1>Kesh is an incentive marketing app, where users share ads from available stores to earn discounts</h1>|

| ![alt text](https://docs.google.com/uc?id=1ZnT_Kt9G1HCciqBhVB5EBmHGEbz-ImDu) |
| ------ |
| Step 1: Images presented to users through a catalog |
| Step 2: Users share these images on social networks. The share message will go with a code that will be used to generate a coupon.
| Step 3: The person who received the share enters Kesh and enters the code received, generating a coupon for both people.
| Step 4: The user goes to the store and presents the coupon at the time of payment. A configurable part of this payment returns to the user, being stored in his Kesh wallet and can be used as a form of payment in another purchase. ||


|<h1>Prints</h1>||
| ------ | ------|
|![alt text](https://docs.google.com/uc?id=1YqdY6nCP8lKbdC8QOPlC18Uk3Dxvd6Ii)|![alt text](https://docs.google.com/uc?id=1Gvf6D6FkCKSVQoxqUS0DACJeuaMfrSiL)|
|![alt text](https://docs.google.com/uc?id=1yqIMtXZF1T40u4qpzLfhh28j4RhdVDat)|![alt text](https://docs.google.com/uc?id=1kknei4Aq8y0_9zOqSIrU3eAue-NLd3Hr)|
